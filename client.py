from flask_cors import CORS
from flask import Flask, render_template, jsonify

app = Flask(__name__, static_folder='static')
CORS(app)

@app.route('/', methods=['GET'])
def index():
    return app.send_static_file('html/index.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
