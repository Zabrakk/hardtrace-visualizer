# Hardtrace-Visualizer
Visualizes [Hardtrace](https://gitlab.com/CinCan/hardtrace) session summaries created by the [hardtrace-server](https://gitlab.com/Zabrakk/hardtrace-server). Created with Python and D3.


![Example visualization](img/example.png)

## Getting Started
```bash
git clone https://gitlab.com/Zabrakk/hardtrace-visualizer.git
cd hardtrace-visualizer
pip3 install -r requirements.txt
python3 client.py
```
The visualizer can be then found at http://127.0.0.1:5001.

If you are not running hardtrace-server locally, change the *API_URL* variable in /static/scripts/variables.js to the correct address.