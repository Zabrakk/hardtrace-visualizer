API_URL = "http://127.0.0.1:5000/api"
USERS_AND_SESSIONS = []
FILES = []

SAVE_SCALE = 10
IGNORE_USERS = []
IGNORE_FILES = ""
VIS_SELECTION = ""

SESSION_JSON_TEMPALTE = {
    "graph": true,
    "includes": "users",
    "summarize": [],
    "ignore-files": ""
}

FILE_JSON_TEMPLATE = {
    "graph": true,
    "includes": "files",
    "summarize": [],
    "ignore-users": [],
    "ignore-files": ""
}
