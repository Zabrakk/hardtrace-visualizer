function readUsername() {
    return document.getElementById('username').value;
}

function readToken() {
    return document.getElementById('token').value;
}

function get(URL, func) {
    $.ajax({
        type: 'GET',
        url: URL,
        beforeSend: function(request) {
            request.setRequestHeader('username', readUsername());
            request.setRequestHeader('X-Authorization', readToken());
        },
        success: func,
        error: displayError
    });
}

function post(URL, func, content={}) {
    $.ajax({
        type: 'POST',
        url: URL,
        beforeSend: function(request) {
            request.setRequestHeader('username', readUsername());
            request.setRequestHeader('X-Authorization', readToken());
        },
        data: JSON.stringify(content),
        contentType: 'application/json',
        dataType: 'json',
        success: func,
        error: displayError
    });
}

function displayError(err) {
    alert("Status: " + err.status + ".\nMessage: " + err.responseText);
    console.error(err);
}

async function getSessionNetwork() {
    let json = SESSION_JSON_TEMPALTE;
    json['ignore-files'] = IGNORE_FILES;
    json['summarize'] = [];
    for (var i in USERS_AND_SESSIONS) {
        if (!USERS_AND_SESSIONS[i].selected) continue;
        json['summarize'].push(
            {
                'user': USERS_AND_SESSIONS[i].pseudonym,
                'session': USERS_AND_SESSIONS[i].session
            }
        );
    }
    URL = API_URL + '/analysis/summary/'
    post(URL, drawNetwork, json);
}

async function getFileNetwork() {
    let json = FILE_JSON_TEMPLATE;
    json['ignore-files'] = IGNORE_FILES;
    json['summarize'] = [];
    for (var i in FILES) {
        if (!FILES[i].selected) continue;
        json['summarize'].push(
            {
                'filename': FILES[i].filepath,
                'hash': FILES[i].hash
            }
        );
    }
    json['ignore-users'] = [];
    for (var i in IGNORE_USERS) {
        if (!IGNORE_USERS[i].ignore) continue;
        json['ignore-users'].push(IGNORE_USERS[i].pseudonym);
    }
    URL = API_URL + '/analysis/summary/'
    post(URL, drawNetwork, json);
}
