function drawNetwork(data) {
    draw(data);
}

var legend, svg, width, color;

function setD3Parameters() {
    svg = d3.select(".svgVisualization");           // Actual visualization goes here
    legend = d3.select(".svgLegend");               // Legend goes here
    width = parseFloat(svg.style("width").replace("px", ""));
    height = parseFloat(svg.style("height").replace("px", ""));

    color = d3.scaleOrdinal(); // For edge coloring
} 

function draw(data) {
    svg.selectAll("*").remove();
    legend.selectAll("*").remove();

    var gLegend = legend.append("g");
    var g = svg.append("g");            // Nodes, links and labels added here
    var defs = svg.append("svg:defs");  // Invisible stuff (definitions)

    var zoom = d3.zoom().scaleExtent([0.5, 10]).on("zoom", handleZoom); // Zooming the image
    svg.call(zoom); // Add zoom to the svg
    // Handler function for zooming
    function handleZoom() {
        g.attr("transform", d3.zoomTransform(this));
    }

    pseudonyms = []
    for (var i in data.edges) {
        pseudonym = data.edges[i].run_by;
        if ($.inArray(pseudonym, pseudonyms) == -1) {
            pseudonyms.push(pseudonym)
        }
    }

    // Force-directed
    var simulation = d3
        .forceSimulation(data.nodes)
        //.force("link", d3.forceLink(data.edges))
        .force("link", d3.forceLink(data.edges).distance(40).strength(0.9))
        .force("forceX", d3.forceX(width/2).strength(0.1))
        .force("forceY", d3.forceY(height/2).strength(0.1))
        .force("charge", d3.forceManyBody().strength(-150))
        .force("center", d3.forceCenter(width/2, height/2))
        .on("tick", ticked);
    
    // Set color domain
    color.domain(pseudonyms).range(d3.schemeSet2)

    // Add legend
    var legendDot = gLegend
        .selectAll("LegendDot")
        .data(pseudonyms)
        .enter()
        .append("circle")
        .attr("cx", 10)
        .attr("cy", function(d,i){ return 10 + i*25})
        .attr("r", 7)
        .style("fill", function(d){ return color(d)})

    var legendLabel = gLegend
        .selectAll("LegendLabel")
        .data(pseudonyms)
        .enter()
        .append("text")
        .attr("x", 20)
        .attr("y", function(d,i){ return 12.5 + i*25})
        .text(function(d) {
            return d;
        })
        .attr("text-anchor", "left")
        .attr("font-size", 9)
        .style("alignment-baseline", "middle")

    /*
    * The actual graph
    */
    
    // Create the definitions for link direction markers
    createArrow(defs, "arrow", 14, 0);
    createArrow(defs, "arrow2", 5, 0); // No target node arrow

    // Add the links from data
    var link = g
        .selectAll("line") // "path"
        .data(data.edges)
        .enter()
        .append("line") // "path"
        .attr("id", function(d) {
            return "line-"+d.index;
        })
        .attr("stroke-width", 3)    // Bigger store width makes mouseover easier
        .attr("marker-end", function(d) {
            target_node = data.nodes[d.target.index];
            if(target_node.name == null) {
                return "url(#arrow2)";
            }
            return "url(#arrow)";
        })
        .attr("shortCmd", function(d) {
            return d.short_cmd;
        })
        .attr("fullCmd", function(d) {
            return d.cmd;
        })
        .style("stroke", function(d) {
            return color(d.run_by);
        })
        .on("mouseover", handleLineMouseOver)
        .on("mouseout", handleLineMouseOut);
    
    // Add the nodes from data
    var node = g
        .selectAll("circle")
        .data(data.nodes)
        .enter()
        .append("circle")
        .attr("class", "FileNode")
        .attr("r", 5)
        //.attr("fill", "orange")
        .attr("stroke", "yellow")
        .attr("fill", function(d) {
            if (d.selected_file) {
                return "purple";
            }
            else if (d.source_node) {
                return "red";
            }
            return "orange";
        })
        .attr("opacity", function(d) {
            if (d.name == null) {
                return 0;
            }
            return 1;
        })
        .call(d3.drag() // Enable dragging
            .on("start", dragStart)
            .on("drag", dragging)
            .on("end", dragEnd)
        )
        //.on("dblclick", unfreeze); // On double click
        .on("contextmenu", unfreeze); // On right click

    function dragStart(event) {
        simulation.alphaTarget(0.2).restart();
    }

    function dragging(event) {
        event.subject.fx = event.x;
        event.subject.fy = event.y;
    }

    function dragEnd(event) {
        simulation.alphaTarget(0); //(0.05).restart();
    }

    function unfreeze(d) {
        d.preventDefault();
        // Leaving if these values are not null, the simulation won't move the nodes
        d.target.__data__.fx = null;
        d.target.__data__.fy = null;
    }

    // Add labels to files (nodes)
    var file_label = g
        .selectAll(".FileLabel") // selectAll(null)
        .data(data.nodes)
        .enter()
        .append("text")
        .attr("class", "FileLabel")
        .text(function(d) {
            return d.name;
        })
        .style("text-anchor", "middle")
        .style("fill", "black")
        .style("text-shadow", "-1.5px 0 yellow, 0 1.5px yellow, 1.5px 0 yellow, 0 -1.5px yellow")
        .style("font-family", "Arial")
        .style("font-size", 5.5)
        .style("font-weight", 300);

    // Add labels to commands (links)
    var cmd_label = g
        .selectAll(".CmdLabel")
        .data(data.edges)
        .enter()
        .append("text")
        .attr("id", function(d) {
            return "line-"+d.index+"-label";
        })
        .attr("class", "CmdLabel")
        .text(function(d) {
            return d.short_cmd;
        })
        .style("text-anchor", "middle")
        .style("fill", "black")
        .style("font-family", "Arial")
        .style("font-size", 3.5)
        .style("font-weight", 300);

    // Force-directed simulation tick definition
    function ticked() {
        var k = 0.5;

        link
            /*
            // Trying to make it kinda look like a tree stuff
            .each(function(d,i) {
                //d.source.x -= 0, d.target.x += 2; // 0, 0.7
            })
            */
            .attr("x1", function(d) {
                return d.source.x;
            })
            .attr("y1", function(d) {
                return d.source.y;
            })
            .attr("x2", function(d) {
                return d.target.x;
            })
            .attr("y2", function(d) {
                return d.target.y;
            })
            /*
            // For path
            .attr("d", function(d) {
                return `M${d.source.x},${d.source.y}A0,0 0 0,1 ${d.target.x},${d.target.y}`
            })
            */

        node
            .attr("cx", function(d) {
                return d.x;
            })
            .attr("cy", function(d) {
                return d.y;
            })

        file_label // Positioned above nodes
            .attr("x", function(d) {return d.x;})
            .attr("y", function (d) {return d.y - 6;});
        
        cmd_label // Positioned in the middle of links
            .attr("x", function(d) {return (d.source.x+d.target.x)/2})
            .attr("y", function(d) {return (d.source.y+d.target.y)/2+7})
    }
}

/*
* Some functions
*/

function createArrow(d, id, refX, refY) {
    d.append("svg:marker")
        .attr("id", id)
        .attr("viewBox", "0 -5 10 10")
        .attr('refX', refX) // 14,  5 
        .attr("refY", refY) // 0,   0
        .attr("markerWidth", 3.5)
        .attr("markerHeight", 3.5)
        .attr("orient", "auto")
        .append("path")
            .attr("d", "M0,-5L10,0L0,5")
            .attr("fill", "blue")
}

function handleLineMouseOver(d, i) {
    var id = d3.select(this).attr("id");
    var fullCmd = d3.select(this).attr("fullCmd").split("<$%&>");
    var label = d3.select("#"+id+"-label")
    label.text("")
    for (i in fullCmd) {
        var tspan = label.append("tspan").text(fullCmd[i])
        if (i > 0)
            tspan
                .attr("x", label.attr("x"))
                .attr("dy", "4");
    }
}

function handleLineMouseOut(d, i) {
    var id = d3.select(this).attr("id");
    d3.select("#"+id+"-label")
        .text(d3.select(this).attr("shortCmd"));
}
