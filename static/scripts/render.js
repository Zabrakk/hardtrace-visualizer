// Do these when the browser window is loading
window.onload = async function() {
    renderTitle();
    renderAuthentication();
    renderSelectionTool();
    renderSettings();
    renderButtons();

    setD3Parameters();

}

/*
* onLoad rendering
*/

function renderTitle() {
    $(".TitleSection").html(
        "<div class='TitleText'>Hardtrace Visualizer</div>"
    );
}

function renderAuthentication() {
    $(".AuthenticationTitle").html(
        "<h3>Authorization</h3>"
    );
    $(".UsernameText").html(
        "Username: "
    );
    $(".TokenText").html(
        "Token: "
    );
    $(".UsernameField").html(
        "<input type='text' id='username' value='' required/>"
    );
    $(".TokenField").html(
        "<input type='password' id='token' value='' required/>"
    );
    $(".AuthBtn").html(
        "<button class='SelectButton' onClick='update()'>Check</button>"
    )
}

function renderSelectionTool() {
    $(".SelectButtons").html(
        "<button class='SelectButton' onClick='getUsersAndSessions()'>Sessions</button>" +
        "<button class='SelectButton' onClick='getFiles()'>Files</button>" +
        "<button class='SelectButton' onClick='update()'>Update</button>"
    );
    renderSearchSessions();
}

function renderButtons() {
    $(".Buttons").html(
        "<button class='ConstantButton' onClick='showSettings()'>Settings</button>" +
        "<button class='ConstantButton' onClick='visualize()'>Visualize</button>" +
        "<button class='ConstantButton' onClick='downloadThis()'>Download</button>"
    );
}

function renderSettings() {
    $(".SenttingsContent").html(
        "<div><span class='CloseSettings'>&times;</span></div>" +
        "<h3>Common Settings</h3>" +
        "<div class='SettingsTitleText'>Ignore Files:</div>" +
        "<div class='IgnoreFilesInput'><input type='text' id='ignore-files' size='70' placeholder='hash, filename, *.ext, *filename.ext' onInput='ignoreFilesUpdate()' value='" + IGNORE_FILES + "'/></div>" +
        "<div class='SettingsTitleText'>Image Save Scale:</div>" +
        "<div class='ScaleSliderContainer'>" +
            "<input type='range' class='ScaleSlider' id='ScaleSlider' min='1' max='10' value='" + SAVE_SCALE + "' oninput='updateScale()'/>" +
            "<div class='ScaleText'>Scale set to " + SAVE_SCALE + "</div>" +
        "</div>" +
        "<h3>File Visualization Settings</h3>" +
        "<div class='SettingsTitleText'>Ignore Users:</div>" +
        "<div class='IgnoreUsers'></div>"

    );
}

/*
* Dynamically called rendering
*/

function showSettings() {
    console.log("Render settings");
    var close = document.getElementsByClassName("CloseSettings")[0];
    var popup = document.getElementById("SettingsPopup");
    close.onclick = function() {
        popup.style.display = "none";
    }
    popup.style.display = "block";
    renderIngoreUsers();
}

function renderIngoreUsers() {
    var ctr = 0;
    $(".IgnoreUsers").html("<div class='IgnoreRow'>");
    for (var ind in IGNORE_USERS) {
        var user = IGNORE_USERS[ind];
        var style = 'IgnoreBtn';
        if (user.ignore) {
            style = 'IgnoredBtn';
        }
        $(".IgnoreUsers").append(
            "<button class='" + style + "' onClick='ignore(" + ind + ")'>" + user.pseudonym + "</button>"
        );
        ctr++;
        if (ctr >= 7) {
            $(".IgnoreUsers").append("</div> <div class='IgnoreRow'>");
            ctr = 0;
        }
    }
    
}

function renderSearchSessions() {
    $(".SearchField").html(
        "<div class='SearchText1'>" +
            "User:" +
        "</div>" +
        "<div class='SearchBar1'>" +
            "<input type='text' id='user-search' oninput='sessionSearch()'/>" +
        "</div>" +
        "</br>" +
        "<div class='SearchText2'>" +
            "Session:" +
        "</div>" +
        "<div class='SearchBar2'>" +
            "<input type='text' id='session-search' oninput='sessionSearch()'/>" +
        "</div>"
    );
}

function renderSearchFiles() {
    $(".SearchField").html(
        "<div class='SearchText1'>" +
            "Filename:" +
        "</div>" +
        "<div class='SearchBar1'>" +
            "<input type='text' id='filename-search'/ oninput='fileSearch()'>" +
        "</div>" +
        "</br>" +
        "<div class='SearchText2'>" +
            "Hash:" +
        "</div>" +
        "<div class='SearchBar2'>" +
            "<input type='text' id='hash-search' oninput='fileSearch()'/>" +
        "</div>"
    )
}

function _renderUserSession(pseudonym, session, selected, id) {
    divClass = 'SessionItem';
    if (selected) divClass = 'SessionItemSelected';
    $(".SessionSelectField").append(
        "<div class='"+divClass+"' id='session-"+id+"' onClick='selectSession("+id+")'>" +
            "<p>" + pseudonym + ": " + session + "</p>" +
        "</div>"
    );
}

function renderUsersAndSessions(u_and_s) {
    $(".FileSelectField").html("");
    document.getElementById("fileSelect").className = "";
    document.getElementById("sessionSelect").className = "SessionSelectField";
    $(".SessionSelectField").html("");
    for (var i in u_and_s) {
        let entry = u_and_s[i];
        _renderUserSession(entry.pseudonym, entry.session, entry.selected, entry.id);
    }
}

function _renderFile(filepath, hash, created_at, selected, id) {
    divClass = 'FileItem';
    if (selected) divClass = 'FileItemSelected';
    $(".FileSelectField").append(
        "<div class='"+divClass+"' id='file-"+id+"'onClick='selectFile("+id+")'>" +
            "<p>" + filepath + "</p>" +
            "<p>" + hash + "</p>" +
            "<p>" + created_at + "</p>" +
        "</div>"
    );
}

function renderFiles(files) {
    $(".SessionSelectField").html("");
    document.getElementById("sessionSelect").className = "";
    document.getElementById("fileSelect").className = "FileSelectField";
    $(".FileSelectField").html("");
    for (var i in files) {
        let entry = files[i];
        _renderFile(entry.filepath, entry.hash, entry.created_at, entry.selected, entry.id);
    }

}
