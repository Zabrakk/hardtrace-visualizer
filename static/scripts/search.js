function sessionSearch() {
    var user_search_term = document.getElementById("user-search").value;
    var session_search_term = document.getElementById("session-search").value;
    var result = []

    if (user_search_term.length > 0 && session_search_term.length == 0) {
        for(var i in USERS_AND_SESSIONS) {
            if (USERS_AND_SESSIONS[i].pseudonym.includes(user_search_term)) {
                result.push(USERS_AND_SESSIONS[i]);
            }
        }

    } else if (session_search_term.length > 0 && user_search_term.length == 0) {
        for(var i in USERS_AND_SESSIONS) {
            if (USERS_AND_SESSIONS[i].session.includes(session_search_term)) {
                result.push(USERS_AND_SESSIONS[i]);
            }
        }

    } else if (user_search_term.length > 0 && session_search_term.length > 0) {
        for(var i in USERS_AND_SESSIONS) {
            if (USERS_AND_SESSIONS[i].pseudonym.includes(user_search_term) && USERS_AND_SESSIONS[i].session.includes(session_search_term)) {
                result.push(USERS_AND_SESSIONS[i]);
            }
        }

    } else {
        console.log(USERS_AND_SESSIONS);
        renderUsersAndSessions(USERS_AND_SESSIONS);
        return;
    }
    renderUsersAndSessions(result);
}

function fileSearch() {
    var filename_search_term = document.getElementById("filename-search").value;
    var hash_search_term = document.getElementById("hash-search").value;
    var result = []

    if (filename_search_term.length > 0 && hash_search_term.length == 0) {
        for(var i in FILES) {
            if (FILES[i].filepath.includes(filename_search_term)) {
                result.push(FILES[i]);
            }
        }

    } else if (hash_search_term.length > 0 && filename_search_term.length == 0) {
        for(var i in FILES) {
            if (FILES[i].hash.includes(hash_search_term)) {
                result.push(FILES[i]);
            }
        }

    } else if (filename_search_term.length > 0 && hash_search_term.length > 0) {
        for(var i in FILES) {
            if (FILES[i].filepath.includes(filename_search_term) && FILES[i].hash.includes(hash_search_term)) {
                result.push(FILES[i]);
            }
        }

    } else {
        renderFiles(FILES);
        return;
    }
    renderFiles(result);
}
