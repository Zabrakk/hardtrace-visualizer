function update() {
    console.log("Updating users...");
    URL = API_URL + '/analysis/user-info/';
    get(URL, _updateUsersAndSessions);
    
    console.log("Updating files");
    URL = API_URL + '/analysis/file-info/';
    get(URL, _updateFiles);
}

function _updateUsersAndSessions(data) {
    USERS_AND_SESSIONS = [];
    IGNORE_USERS = [];
    var i = 0, j = 0;
    for (var pseudonym in data) {
        for (var session_index in data[pseudonym]) {
            USERS_AND_SESSIONS.push(
                {
                    'id': i,
                    'pseudonym': pseudonym,
                    'session': data[pseudonym][session_index],
                    'selected': false
                }
            );
            i++;
        }
        IGNORE_USERS.push(
            {
                'id': j,
                'pseudonym': pseudonym,
                'ignore': false
            }
        )
    }
}

function _updateFiles(data) {
    FILES = []
    for (var i in data) {
        let entry = data[i];
        entry.id = i;
        entry['selected'] = false;
        FILES.push(entry); 
    }
}

function downloadThis() {
    console.log("Downloadin");
    //saveSvg(document.getElementById("vis"), "diagram.svg");
    saveSvgAsPng(document.getElementById("vis"), "diagram.png", {scale: SAVE_SCALE, backgroundColor: "white"})
}

function ignore(ind) {
    IGNORE_USERS[ind].ignore = !IGNORE_USERS[ind].ignore;
    renderIngoreUsers();
}

function getUsersAndSessions() {
    renderSearchSessions();
    renderUsersAndSessions(USERS_AND_SESSIONS);
}

function selectSession(i) {
    VIS_SELECTION = "sessions"
    USERS_AND_SESSIONS[i].selected = !USERS_AND_SESSIONS[i].selected;
    if (USERS_AND_SESSIONS[i].selected) {
        document.getElementById("session-"+i).className = "SessionItemSelected";
    } else {
        document.getElementById("session-"+i).className = "SessionItem";
    }
    //getSessionNetwork(); // async
}

function getFiles() {
    renderSearchFiles();
    renderFiles(FILES);
}

function selectFile(i) {
    VIS_SELECTION = "files"
    FILES[i].selected = !FILES[i].selected;
    if (FILES[i].selected) {
        document.getElementById("file-"+i).className = "FileItemSelected"
    } else {
        document.getElementById("file-"+i).className = "FileItem"
    }
    //getFileNetwork(); // async
}

function updateScale() {
    var slider = document.getElementById("ScaleSlider");
    SAVE_SCALE = slider.value;
    $(".ScaleText").html(
        "Scale set to " + SAVE_SCALE
    );
}

function ignoreFilesUpdate() {
    var input = document.getElementById("ignore-files");
    IGNORE_FILES = input.value;
}

function visualize() {
    if (VIS_SELECTION == "sessions") {
        getSessionNetwork();
    } else if (VIS_SELECTION == "files") {
        getFileNetwork();
    }
}